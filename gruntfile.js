module.exports = function(grunt) {

// Project configuration.
grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),

  compass: {
    dist: {
      options: {
        sassDir: 'sass',
        cssDir: 'css'
      }
    }
  },
  watch: {
      css: {
          files: ['sass/*.scss'],
          tasks: ['compass']
      }
  }
});

// Load the Grunt plugins.
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-compass');

// Register the default tasks.
grunt.registerTask('default', ['watch']);
};