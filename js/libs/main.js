$(document).ready(function(){
  $('.form-submit').on('click', function(e){
    var selectedNPS = $("input[type='radio'][name='radio']:checked");
    var email = $('#email').val();

    emailAlert(e, validateEmail(email));
    validateNPS(e, selectedNPS);
  })

  function validateNPS(e,selectedNPS){
    if (selectedNPS.length == 0) {
      e.preventDefault();
      $('.message-nps').removeClass('hide');
      $('.nps-q-a').addClass('nps-q-a-alert');
    } else {
      console.log('verstuur!');
    }
  }

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function emailAlert(e, isValid) {
    if(isValid === false) {
      e.preventDefault();
     $('.message-email').removeClass('hide');
     $('.input-email').addClass('input-email-alert')
    }
  }
})